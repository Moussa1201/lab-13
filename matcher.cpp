#include <iostream>
#include "CharStack.cpp"
#include "PairMatcher.cpp"

using namespace std;

int main() {
    PairMatcher matcher('(', ')');
    
    string testString = "(())";
    
    cout << testString << " is " << (matcher.check(testString) ? "valid" : "invalid") << endl;
    return 0;
}
