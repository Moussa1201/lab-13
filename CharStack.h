#include <iostream>
using namespace std;

class CharStack {
private:
	char *stack;
	int max;
	int size=0;
public:
	CharStack(int newMax);
	~CharStack();
	bool push(char c);
	char pop();
	bool isEmpty() const;
};